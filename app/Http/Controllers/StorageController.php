<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StorageController extends Controller
{
    //

    public function store(Request $request)
    {
        $path = $request->file('img')->store('public/img');
        $data['image'] = $path;
        return view('show', $data);
    }
}
