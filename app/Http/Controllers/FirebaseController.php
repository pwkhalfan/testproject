<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Database;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Storage;
class FirebaseController extends Controller
{
    //
    public $database;
    public $tableName;
    public $auth;
    public $storage;

    public function __construct(Database $database, Auth $auth, Storage $storage)
    {
        $this->database = $database;
        $this->tableName = 'contacts';
        $this->auth = $auth;
        $this->storage = $storage;
    }

    public function store(Request $request)
    {
        $postData = [
            'name' =>  $request->name,
            'phone' => $request->phone,
            'email' => $request->email
        ];
        $postRef = $this->database->getReference($this->tableName)->push($postData);
        if($postRef)
        {
            return response()->json([
                'success' => true,
                'description' => 'Contact Added Successfully'
            ]);
        }

        return response()->json([
            'success' => false,
            'description' => 'Cant add Contact'
        ]);
    }

    public function index()
    {
        $contacts = $this->database->getReference($this->tableName)->getValue();
        return response()->json([
            'success' => true,
            'data'  => $contacts
        ]);
    }

    public function verify(Request $request)
    {
        $signInResult = $this->auth->signInWithEmailAndPassword($request->email, $request->password);

        return response()->json([
            'success' => true,
            'data'  => $signInResult
        ]);
    }

    public function createUser()
    {
        $userProperties = [
            'email' => 'a@a.com',
            'emailVerified' => false,
            'phoneNumber' => '+96896919868',
            'password' => '12345678',
            'displayName' => 'Eng. Khalfan Alamri',
            'photoUrl' => 'http://www.example.com/12345678/photo.png',
            'disabled' => false,
        ];

        return $createdUser = $this->auth->createUser($userProperties);
    }

    public function storage(Request $request)
    {

        $defaultBucket = $this->storage->getBucket();
        $storage = $this->storage->getStorageClient();
//        $storage = new StorageClient();
        $file = $request->file;
//        $bucket = $storage->bucket($defaultBucket);
        $bucket = $this->storage->getBucket();
        $object = $bucket->upload($file, [
            'name' => 'laravel-img12.png'
        ]);


        printf('Uploaded %s to gs://%s/%s' . PHP_EOL, basename($file ), $defaultBucket, 'laravel-img12.png');

        return "Uploaded";
//        printf('Uploaded %s to gs://%s/%s' . PHP_EOL, basename($source), $bucketName, $objectName);

//        return $storageClient = $this->storage->getStorageClient();
//        $defaultBucket = $this->storage->getBucket();
//
//        $this->storage->
    }
}
