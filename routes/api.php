<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/contacts/store', [\App\Http\Controllers\FirebaseController::class, 'store']);
Route::get('/contacts/index', [\App\Http\Controllers\FirebaseController::class, 'index']);

Route::post('/verify', [\App\Http\Controllers\FirebaseController::class, 'verify']);

Route::post('/createUser', [\App\Http\Controllers\FirebaseController::class, 'createUser']);

Route::post('/storage', [\App\Http\Controllers\FirebaseController::class, 'storage']);




